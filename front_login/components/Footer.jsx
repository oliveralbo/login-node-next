import React from "react";

class Footer extends React.Component {
  render() {
    return (
      <div>
        <footer>
          <div className="paper">
            <p className="foot">
              © 2019 - All Rights reserved - EHO - Saenz Peña. Argentina.
            </p>
          </div>
        </footer>

        <style jsx>{`
          .paper {
            color: white;
            background-color: black;
            position: fixed;
            width: 99%;
            height: 3%;
            bottom: 0;
            border-top: 1px solid #808080;
          }
          .foot {
            margin: 0;
          }
        `}</style>
      </div>
    );
  }
}
export default Footer;
