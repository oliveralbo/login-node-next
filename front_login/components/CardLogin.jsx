import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Router from "next/router";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Link from "next/link";
import classNames from "classnames";
import TextField from "@material-ui/core/TextField";
// import logo from "../static/logo.jpg";

//modales
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";

const styles = theme => ({
  card: {
    marginTop: "10%",
    marginBottom: "4%",
    maxWidth: "25%",
    display: "block",
    marginLeft: "auto",
    marginRight: "auto"
  },
  logo: {
    width: "75%",
    marginLeft: "auto",
    marginRight: "auto",
    display: "block"
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)"
  },
  title: {
    fontSize: 14
  },
  pos: {
    marginBottom: 12
  },
  dense: {
    marginTop: 16
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  },
  link: {
    marginTop: 16
  },
  boton: {
    marginLeft: "70%",
    color: "blue",
    usuario: null
  }
});

class CardLogin extends React.Component {
  state = {
    user: "",
    pass: "",
    usuario: "",
    errorInputs: false
  };

  handleChange = estado => event => {
    this.setState({ [estado]: event.target.value });
  };

  consulta = () => {
    if (this.state.usuario.length != 1) {
      this.setState({ errorInputs: true });
    } else {
      // Router.push("/home");
      let pass = btoa(this.state.pass);
      Router.push({
        pathname: "/home",
        query: { user: this.state.user, pass }
      });
    }
  };

  handleClose = () => {
    this.setState({ errorInputs: false, user: "", pass: "" });
  };

  ingresar = async () => {
    let paquete = {};

    paquete.usuario = this.state.user;
    let pass = btoa(this.state.pass);
    paquete.password = pass;

    await fetch("http://localhost:5000/login", {
      method: "post",
      body: JSON.stringify(paquete)
    })
      .then(function(respuesta) {
        // Convertir a JSON
        return respuesta.json();
      })
      .then(usuario => {
        // Ahora 'usuario' es un objeto JSON
        this.setState({ usuario });
      });

    this.consulta();
  };

  render() {
    const { classes } = this.props;

    return (
      <Card className={classes.card}>
        <CardContent>
          <div className={classes.logo}>
            <img src="../static/logo.jpg" />
          </div>

          <TextField
            error={this.state.errorInputs}
            id="outlined-dense"
            label="USUARIO"
            value={this.state.user}
            onChange={this.handleChange("user")}
            className={classNames(classes.textField, classes.dense)}
            margin="dense"
            variant="outlined"
          />
          <TextField
            error={this.state.errorInputs}
            id="outlined-dense"
            label="CONTRASEÑA"
            value={this.state.pass}
            type="password"
            onChange={this.handleChange("pass")}
            className={classNames(classes.textField, classes.dense)}
            margin="dense"
            variant="outlined"
          />

          <div className={classes.link}>
            <span>No tenés cuenta, </span>
            <Link href="/registro">
              <a>Registrate acá</a>
            </Link>
          </div>
        </CardContent>
        <CardActions>
          <Button
            className={classes.boton}
            onClick={this.ingresar}
            size="small"
          >
            {" "}
            Ingresar
          </Button>
        </CardActions>

        <Dialog
          open={this.state.errorInputs}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">
            {"Usuario Inexistente !!"}
          </DialogTitle>

          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              <p>Usuario o contraseña incorrectos.</p>
              <p>Intente nuevamente. Si no tiene usuario, registrese</p>
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary" autoFocus>
              Aceptar
            </Button>
          </DialogActions>
        </Dialog>

        <style jsx>{`
          p {
            color: red;
            margin: 0;
            padding: 0;
          }
        `}</style>
      </Card>
    );
  }
}

export default withStyles(styles)(CardLogin);
