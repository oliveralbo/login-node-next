import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Router from "next/router";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";

// function TabContainer(props) {
//   return (
//     <Typography component="div" style={{ padding: 8 * 3 }}>
//       {props.children}
//     </Typography>
//   );
// }

const styles = {
  root: {
    flexGrow: 1
  },
  bar: {
    backgroundColor: "#184ECB"
  },
  grow: {
    flexGrow: 0.1
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  },
  tabs: {
    flexGrow: 1
  },
  menuUser: {
    float: "right",
    color: "red",
    left: "85%"
  }
};

class MenuApp extends React.Component {
  state = {
    value: 0,
    menu: null
  };

  handleClick = event => {
    this.setState({ menu: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ menu: null });
  };

  sessionClose = () => {
    this.setState({ menu: null });
    Router.push("/");
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };
  render() {
    let userName;
    if (this.props.usuario[0]) {
      userName = this.props.usuario[0].user_name;
    }
    const { value } = this.state;
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar className={classes.bar}>
            <Typography variant="h5" color="inherit" className={classes.grow}>
              My App
            </Typography>
            <Tabs
              value={value}
              className={classes.tabs}
              onChange={this.handleChange}
            >
              <Tab label="Home" />
              <Tab label="Consulta" />
              <Tab label="Imagenes" />
            </Tabs>
            <Button
              onClick={this.handleClick}
              color="inherit"
              aria-owns={this.state.menu ? "simple-menu" : undefined}
              aria-haspopup="true"
            >
              {userName}
            </Button>
          </Toolbar>
        </AppBar>
        {/* esto seria solo el titulo y elcontenido d cada pagina hay q hacerlo en pages */}
        {value === 0 && <h3>Home</h3>}
        {value === 1 && <h3>Consulta</h3>}
        {value === 2 && <h3>Imagenes</h3>}

        <Menu
          className={classes.menuUser}
          id="simple-menu"
          menu={this.state.menu}
          open={this.state.menu}
          onClose={this.handleClose}
        >
          <MenuItem onClick={this.handleClose}>Otra Opcion</MenuItem>
          <MenuItem onClick={this.handleClose}>Mi Cuenta</MenuItem>
          <MenuItem onClick={this.sessionClose}>Cerrar sesión</MenuItem>
        </Menu>
      </div>
    );
  }
}

export default withStyles(styles)(MenuApp);
