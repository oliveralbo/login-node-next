import React, { Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Link from "next/link";
import classNames from "classnames";
import TextField from "@material-ui/core/TextField";
//modales
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";

import Checkbox from "@material-ui/core/Checkbox";

//estilos con styles js

const styles = theme => ({
  card: {
    marginTop: "10%",
    marginBottom: "4%",
    maxWidth: "95%",
    padding: "3%",
    // display: "block",
    marginLeft: "5%",
    marginRight: "5%"
  },
  logo: {
    width: "10%",
    float: "left"
  },
  h3: {
    marginLeft: "40%"
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)"
  },
  title: {
    fontSize: 14
  },
  pos: {
    marginBottom: 12
  },
  dense: {
    marginTop: 16
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  },
  link: {
    marginTop: 16
  },
  terminos: {
    margin: "0%",
    marginLeft: "3%",
    width: "105%",
    padding: "3%"
  },
  volver: {
    margin: "0%",
    marginRight: "20%",
    width: "105%",
    padding: "30%"
  },
  button: {
    margin: theme.spacing.unit,
    float: "right",
    marginLeft: "63%"
  }
});

class PanelRegistro extends React.Component {
  state = {
    nombre: "",
    errorNombre: false,
    apellido: "",
    errorApellido: false,
    usuario: "",
    errorUsuario: false,
    email: "",
    errorMail: false,
    password: "",
    errorPass: false,
    passwordRepeat: "",
    errorPassRep: false,
    paquete: "",
    openError: false,
    openOK: false,
    checkedB: false
  };

  // componentDidMount() {
  //   var string = "oliverio";

  //   // Encode the String
  //   var encodedString = btoa(string);
  //   let deco = atob(encodedString);
  //   console.log(encodedString);
  //   console.log(deco);
  // }

  handleChange = estado => event => {
    this.setState({ [estado]: event.target.value }, () => {
      if (estado === "nombre") {
        if (this.state.nombre.length > 15 || this.state.nombre.length < 3) {
          this.setState({ errorNombre: true });
        } else {
          this.setState({ errorNombre: false });
        }
      }
      if (estado === "apellido") {
        if (this.state.apellido.length > 15 || this.state.apellido.length < 3) {
          this.setState({ errorApellido: true });
        } else {
          this.setState({ errorApellido: false });
        }
      }
      if (estado === "usuario") {
        if (this.state.usuario.length > 15 || this.state.usuario.length < 3) {
          this.setState({ errorUsuario: true });
        } else {
          this.setState({ errorUsuario: false });
        }
      }
      if (estado === "email") {
        if (
          this.state.email.indexOf("@") != -1 &&
          this.state.email.indexOf(".") != -1
        ) {
          this.setState({ errorMail: false });
        } else {
          this.setState({ errorMail: true });
        }
      }
      if (estado === "password") {
        if (this.state.password.length > 15 || this.state.password.length < 3) {
          this.setState({ errorPass: true });
        } else {
          this.setState({ errorPass: false });
        }
      }
      if (estado === "passwordRepeat") {
        if (this.state.password != this.state.passwordRepeat) {
          this.setState({ errorPassRep: true });
        } else {
          this.setState({ errorPassRep: false });
        }
      }
    });
  };

  handleChangeCheck = name => event => {
    this.setState({ [name]: event.target.checked });
  };

  enviar = () => {
    if (
      this.state.errorNombre === false &&
      this.state.nombre != "" &&
      this.state.errorApellido === false &&
      this.state.apellido != "" &&
      this.state.errorUsuario === false &&
      this.state.usuario != "" &&
      this.state.errorMail === false &&
      this.state.email != "" &&
      this.state.errorPass === false &&
      this.state.password != "" &&
      this.state.errorPassRep === false &&
      this.state.passwordRepeat != ""
    ) {
      let paquete = {};
      paquete.nombre = this.state.nombre;
      paquete.apellido = this.state.apellido;
      paquete.usuario = this.state.usuario;
      paquete.email = this.state.email;
      let pass = btoa(this.state.password);
      paquete.password = pass;
      let fechaDate = new Date();
      let create = fechaDate.toString();
      paquete.creado = create;
      this.setState({ paquete });
      // console.log(this.state.paquete);

      fetch("http://localhost:5000/insertarUsuario", {
        method: "post",
        body: JSON.stringify(paquete)
      })
        .then(function(res) {
          return console.log(res);
        })
        .then(function(data) {
          console.log(JSON.stringify(data));
        });
      // console.log(paquete);
      // console.log(
      //   "se a registrado - descomente el fecth y comente este console.log"
      // );

      this.setState({
        openOK: true,
        nombre: "",
        apellido: "",
        usuario: "",
        email: "",
        password: "",
        passwordRepeat: ""
      });
    } else {
      this.setState({ openError: true });
    }
  };

  // traer = () => {
  //   fetch("http://localhost:5000/traerUsuarios")
  //     .then(response => response.json())
  //     .then(data => this.setState({ data }))
  //     .catch(function(error) {
  //       console.log("Request failed", error);
  //     });
  //   console.log(this.state.data);
  // };

  handleClose = () => {
    this.setState({ openError: false, openOK: false });
  };

  render() {
    const { classes } = this.props;

    return (
      <Fragment>
        <Card className={classes.card}>
          <CardContent>
            <div>
              <img className={classes.logo} src="../static/logo.jpg" />
            </div>
            <h1 className={classes.h3}>REGISTRO</h1>
            <form className={classes.container} noValidate autoComplete="on">
              <TextField
                error={this.state.errorNombre}
                value={this.state.nombre}
                id="name"
                type="text"
                fullWidth
                label="nombre"
                onChange={this.handleChange("nombre")}
                className={classNames(classes.textField, classes.dense)}
                margin="dense"
              />

              <TextField
                error={this.state.errorApellido}
                value={this.state.apellido}
                id="apellido"
                type="text"
                fullWidth
                label="apellido"
                onChange={this.handleChange("apellido")}
                className={classNames(classes.textField, classes.dense)}
                margin="dense"
              />

              <TextField
                error={this.state.errorUsuario}
                value={this.state.usuario}
                id="standard-dense"
                fullWidth
                label="usuario"
                onChange={this.handleChange("usuario")}
                className={classNames(classes.textField, classes.dense)}
                margin="dense"
              />
              <TextField
                error={this.state.errorMail}
                value={this.state.email}
                id="standard-dense"
                type="email"
                fullWidth
                label="e-mail"
                onChange={this.handleChange("email")}
                className={classNames(classes.textField, classes.dense)}
                margin="dense"
              />
              <TextField
                error={this.state.errorPass}
                value={this.state.password}
                id="standard-dense"
                fullWidth
                label="password"
                onChange={this.handleChange("password")}
                type="password"
                autoComplete="current-password"
                className={classNames(classes.textField, classes.dense)}
                margin="dense"
              />
              <TextField
                error={this.state.errorPassRep}
                value={this.state.passwordRepeat}
                id="standard-dense"
                fullWidth
                label="repetir password"
                onChange={this.handleChange("passwordRepeat")}
                type="password"
                autoComplete="current-password"
                className={classNames(classes.textField, classes.dense)}
                margin="dense"
              />
            </form>
          </CardContent>

          <CardActions>
            <div className={classes.link}>
              <p>
                <Checkbox
                  checked={this.state.checkedB}
                  onChange={this.handleChangeCheck("checkedB")}
                  value="checkedB"
                  color="primary"
                />
                <span>Acepto los </span>
                <Link href="/terminosYCondiciones">
                  <a>
                    <strong>Terminos y condiciones</strong>
                  </a>
                </Link>
              </p>
              <p className={classes.terminos}>
                <span>Si queres, </span>
                <Link href="/">
                  <a>Volver.</a>
                </Link>
              </p>
            </div>
            <div className={classes.button}>
              <Button
                disabled={!this.state.checkedB}
                variant="contained"
                onClick={this.enviar}
                color="primary"
                size="small"
              >
                Registarse
              </Button>
              {/* <Button
              variant="contained"
              onClick={this.traer}
              color="primary"
              size="small"
            >
              traer
            </Button> */}
            </div>
          </CardActions>
        </Card>

        <Dialog
          open={this.state.openError || this.state.openOK}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          {this.state.openOK ? (
            <DialogTitle id="alert-dialog-title">
              {"Registro exitoso !!"}
            </DialogTitle>
          ) : (
            <DialogTitle id="alert-dialog-title">
              {"Registro incorrecto"}
            </DialogTitle>
          )}

          <DialogContent>
            {this.state.openOK ? (
              <DialogContentText id="alert-dialog-description">
                Se a registrado correctamente, ingrese "usuario" y "password"
                para ingresar.
              </DialogContentText>
            ) : (
              <DialogContentText id="alert-dialog-description">
                Debe llenar todos los campos y cumplir sus requisitos
              </DialogContentText>
            )}
          </DialogContent>
          <DialogActions>
            {this.state.openOK ? (
              <Link href="/">
                <Button onClick={this.handleClose} color="primary" autoFocus>
                  Aceptar
                </Button>
              </Link>
            ) : (
              <Button onClick={this.handleClose} color="primary" autoFocus>
                Aceptar
              </Button>
            )}
          </DialogActions>
        </Dialog>
      </Fragment>
    );
  }
}

export default withStyles(styles)(PanelRegistro);
