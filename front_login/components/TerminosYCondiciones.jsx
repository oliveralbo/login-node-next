import React, { Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Link from "next/link";
import Router from "next/router";
import classNames from "classnames";
import TextField from "@material-ui/core/TextField";

//estilos con styles js

const styles = theme => ({
  card: {
    marginTop: "10%",
    marginBottom: "4%",
    maxWidth: "95%",
    display: "block",
    marginLeft: "auto",
    marginRight: "auto"
  },
  logo: {
    width: "10%",
    float: "left"
  },
  h3: {
    marginLeft: "25%"
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)"
  },
  title: {
    fontSize: 14
  },
  pos: {
    marginBottom: 12
  },
  dense: {
    marginTop: 16
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  },
  link: {
    marginTop: 16
  },
  button: {
    margin: theme.spacing.unit,
    float: "right",
    marginLeft: "90%"
  }
});

class TerminosYCondiciones extends React.Component {
  volver = () => {
    Router.push("/registro");
  };
  render() {
    const { classes } = this.props;

    return (
      <Fragment>
        <Card className={classes.card}>
          <CardContent>
            <div>
              <img className={classes.logo} src="../static/logo.jpg" />
            </div>
            <h1 className={classes.h3}>TERMINOS Y CONDICIONES</h1>
            Los siguientes términos y condiciones (“los Términos y Condiciones”)
            regulan el uso del Sitio decero.com, decerowine.com,
            decerowinery.com and theowlandthedustdevil.com (en adelante
            denominados indistintamente como “el Sitio”) donde Finca Decero
            brinda información general sobre la compañía y sus actividades. El
            uso del Sitio por Ud. (“el Usuario”), indica la aceptación absoluta
            de todos y cada uno de los Términos y Condiciones. Su objetivo es
            definir las condiciones de acceso, navegación y uso del Sitio. Los
            Términos y Condiciones se establecen sin perjuicio de los términos y
            condiciones de uso de la plataforma que ofrece el Sitio y no
            pretenden contravenir, reemplazar o sustituir los términos y
            condiciones de uso de tal plataforma. El Usuario se obliga a cumplir
            con todas las disposiciones contenidas en los Términos y
            Condiciones, bajo las leyes aplicables, estatutos, reglamentos y
            regulaciones concernientes al uso del Sitio. El titular del Sitio se
            reserva el derecho de revisar estos Términos y Condiciones en
            cualquier momento, actualizando y/o modificando esta página. Si el
            Usuario no estuviera de acuerdo con estos Términos y Condiciones
            deberá abstenerse de acceder y de utilizar este Sitio. Finca Decero,
            conforme a la ley 24.788, ofrece este medio sólo a quienes hayan
            alcanzado la edad legal para consumir y/o comprar bebidas
            alcohólicas (la que sea mayor) en su país de residencia y siempre
            que el consumo y/o venta de bebidas alcohólicas sea legal en ese
            país. Si no existen tales leyes en su país de residencia, el Usuario
            deberá ser mayor de 21 años para acceder al Sitio. Se recomienda a
            los padres o representantes legales de menores controlar y
            supervisar su acceso a la web y a los sitios visitados por ellos
            para evitar el acceso a contenidos y/o información que pudiera ser
            perjudicial para su adecuado desarrollo. El sitio de Finca Decero
            requiere para habilitar el ingreso que el Usuario declare – en
            carácter de declaración jurada- que tiene más de 18 años y/o la edad
            legal para adquirir o consumir alcohol en el país desde el cual
            accede al Sitio.
            <p>
              {" "}
              1.1. Acceso y utilización del Sitio. El acceso y utilización del
              Sitio no exige la previa suscripción o registro del Usuario. Sin
              perjuicio de ello, en caso de que la utilización de algunos
              servicios ofrecidos a través del Sitio requieran la suscripción o
              registro del Usuario, éste será debidamente advertido, y será bajo
              su responsabilidad continuar con el servicio que ofrece el Sitio.
            </p>
            <p>
              {" "}
              1.2. Utilización del Sitio. El Usuario se compromete a utilizar el
              Sitio de conformidad con la ley, los Términos y Condiciones
              Generales así como con la moral y buenas costumbres y de acuerdo
              al orden público. Queda prohibida cualquier forma de reproducción,
              distribución, exhibición, transmisión, emisión, almacenamiento,
              digitalización, puesta a disposición de terceros, traducción,
              adaptación, o cualquier otro tipo de acto a través del cual el
              Usuario pueda servirse directa o indirectamente, en forma total o
              parcial, de cualquiera de los contenidos del Sitio. El Usuario se
              obliga a abstenerse de utilizar el Sitio con fines o efectos
              ilícitos, contrarios a lo establecido en los Términos y
              Condiciones, lesivos de los derechos e intereses de terceros, o
              que de cualquier forma puedan dañar, inutilizar, sobrecargar o
              deteriorar el Sitio o impedir su normal utilización por parte de
              otros Usuarios. El Usuario responderá por los daños y perjuicios
              de toda naturaleza que Finca Decero pueda sufrir, directa o
              indirectamente, como consecuencia del incumplimiento de cualquiera
              de las obligaciones derivadas de estos Términos y Condiciones, de
              los términos y condiciones de algún servicio en particular que se
              incluya en el Sitio o de la ley aplicable en relación con la
              utilización del Sitio. La obtención y cuidado del hardware y
              software necesarios para acceder al Sitio es de exclusiva
              responsabilidad del Usuario. Finca Decero se reserva el derecho, a
              su absoluta discreción, de suspender o cancelar el acceso o la
              navegación a todo o parte del Sitio y/o su contenido y/o los
              servicios disponibles sin previo aviso y sin que ello dé derecho
              al Usuario a reclamar una indemnización o compensación.
            </p>{" "}
            <p>
              1.3. Utilización del contenido del Sitio. Finca Decero autoriza al
              Usuario a visualizar y/o interactuar con los contenidos del Sitio
              exclusivamente para su uso personal y no comercial. Los contenidos
              de este Sitio, su texto, gráficos, imágenes, logos, iconos de
              botón, software, imágenes, patentes, música, bases de datos y
              cualquier otro material, (“los Contenidos”) están protegidos por
              la legislación sobre propiedad industrial e intelectual, derechos
              de autor y marcas. Los Contenidos son de propiedad exclusiva de
              Finca Decero o de terceros autorizados, cuyos derechos han sido
              licenciados o cedidos a Finca Decero. El Usuario no podrá vender o
              modificar los Contenidos o reproducirlos, exhibirlos,
              representarlos en público, distribuirlos, o hacer otro uso de los
              Contenidos con fines comerciales o de difusión. Está prohibido el
              uso de los Contenidos en cualquier otro sitio de la web. Las fotos
              e imágenes de los productos que se exhiben en el Sitio son solo
              ilustrativas y no son necesariamente la representación exacta del
              producto.
            </p>
            <p>
              {" "}
              1.4. Reglas de Seguridad del Sitio. Se prohíbe al Usuario violar o
              intentar violar la seguridad del Sitio, incluyendo pero no
              limitándose a: (a) acceder a datos que no estén destinados al
              Usuario o entrar en un servidor o cuenta cuyo acceso no está
              autorizado al Usuario, (b) evaluar o probar la vulnerabilidad de
              un sistema o red, o violar las medidas de seguridad o
              identificación sin la adecuada autorización, (c) intentar impedir
              el servicio a cualquier tercero, anfitrión o red, incluyendo sin
              limitación, mediante el envío de virus al Sitio o su implantación,
              o mediante saturación, envíos masivos “spamming”, bombardeo de
              correo o bloqueos del sistema (“crashing”) y (d) enviar correos no
              pedidos, incluyendo promociones y/o publicidad de productos o
              servicios. Las violaciones a la seguridad del Sitio o de la red
              pueden resultar en responsabilidades civiles o penales. Finca
              Decero investigará tales violaciones y puede cooperar con las
              autoridades competentes para perseguir al Usuario involucrado en
              tales violaciones. El Sitio puede incluir enlaces a otros sitios u
              otras fuentes de Internet (incluyendo cualquier fuente propuesta
              por la plataforma que ofrece el medio). Como Finca Decero no puede
              controlar estos sitios y fuentes externas, no se hace responsable
              de la prestación o la exhibición de estos sitios y fuentes
              externas, ni de su contenido, publicidad, productos, servicios o
              cualquier otro material disponible a partir de dichos sitios o
              fuentes externas. Finca Decero no se hace responsable de cualquier
              daño o pérdidas resultantes directa o indirectamente del uso de
              los contenidos, bienes o servicios disponibles en esos sitios o
              fuentes externas.
            </p>
            <p>
              1.5. Usos prohibidos Finca Decero prohíbe específicamente
              cualquier uso del Sitio, (y el uso del Sitio por el Usuario
              implica aceptar esta prohibición) para lo siguiente: Usar
              cualquier mecanismo, software o rutina para impedir o intentar
              impedir el adecuado funcionamiento del Sitio o de cualquier
              actividad que se esté realizando en el Sitio. Realizar cualquier
              acción que imponga una carga desproporcionada o desmesurada sobre
              la infraestructura del Sitio. Si el Usuario posee una contraseña
              que le permite acceder a un área no pública del Sitio, revelar o
              compartir su contraseña con terceras personas o usar su contraseña
              para cualquier propósito no autorizado. No obstante cualquier
              referencia en contrario de estos Términos y Condiciones, usar o
              intentar usar cualquier máquina, software, herramienta, agente u
              otro mecanismo o artilugio para navegar o buscar en este Sitio que
              sean diferentes a los buscadores puestos a disposición por Finca
              Decero en este Sitio que no sean los exploradores web generalmente
              disponibles. Intentar descifrar, descompilar u obtener el código
              fuente de cualquier programa de software que comprenda o
              constituya una parte de este Sitio. Finca Decero se reserva el
              derecho de dar de baja a cualquier Usuario que no cumpla con los
              estándares definidos en estos Términos y Condiciones o con sus
              políticas sin que ello otorgue derecho a resarcimiento alguno.
              Entre las causas de suspensión o exclusión se incluyen (sin
              limitación): Los incumplimientos o violaciones del Usuario a los
              Términos y Condiciones. Cualquier requerimiento de autoridades
              legales o gubernamentales. La solicitud del Usuario. Problemas
              técnicos o de seguridad informática. Períodos de inactividad
              prolongados. Si el Usuario causara la interrupción técnica del
              servicio del Sitio. El Usuario acepta la licitud de cualquier
              causal de terminación enunciada precedentemente y/o cualquiera que
              Finca Decero invoque en el futuro a su exclusivo criterio y
              decisión.
            </p>
            <p>
              {" "}
              1.6. Responsabilidad Finca Decero no garantiza ni asume ningún
              tipo de responsabilidad por los daños y perjuicios sufridos como
              consecuencia del acceso al Sitio por parte de terceros a través de
              conexiones, vínculos, links, hyperlinks o vínculos similares desde
              el Sitio o cualquier otro sitio de propiedad de Finca Decero y/o
              de los sitios enlazados, ni tampoco por enlaces con otros sitios
              que el Usuario pueda realizar desde el Sitio. El Usuario acepta
              voluntariamente que el uso del Sitio, de sus servicios y de los
              Contenidos tiene lugar, en todo caso, bajo su única y exclusiva
              responsabilidad.
            </p>
            <p>
              {" "}
              2.1 Registro del usuario En caso de que el Usuario desee unirse al
              Wine Club de Finca Decero, se le solicitará que provea a Finca
              Decero cierta información que incluirá, entre otros datos, el
              apellido, nombre, número de documento, teléfono, dirección,
              ciudad, código postal, país, dirección de correo electrónico (“la
              Información”). Finca Decero no revelará a terceros la Información
              sin el consentimiento previo del Usuario, excepto en la medida que
              sea necesario para el cumplimiento de las leyes o procedimientos
              legales, cuando tal información sea relevante y dentro del marco
              previsto por la Ley 25.326 de Protección de Datos Personales. Toda
              información o dato personal que el Usuario proporcione a Finca
              Decero será utilizado conforme lo establecido en la Sección
              Política de Privacidad. 2.1 Registro de información Finca Decero
              podrá recopilar del Usuario su dirección de IP, su nombre de
              dominio, la fecha y hora de su visita al Sitio, las páginas
              visitadas dentro del Sitio así como la información descargadas del
              Sitio. También podrá acceder al sitio anterior visitado, el origen
              del tráfico, el tipo de navegador y plataforma utilizada por el
              Usuario. El Usuario al ingresar al Sitio, presta consentimiento a
              la recopilación de mencionada información la cual podrá ser
              utilizada por Finca Decero para recopilar información demográfica,
              para estudios de marketing y para identificar las secciones más
              útiles y populares del Sitio. En ningún caso esta información será
              revelada a terceros sin el previo consentimiento del Usuario. El
              Sitio contiene vínculos a otros sitios (como Facebook, Twitter o
              Vimeo) cuya información, fotos y/o videos son proporcionados por
              Finca Decero. Los vínculos que Finca Decero facilita son
              únicamente para la comodidad del Usuario. Finca Decero solo es
              responsable por las imágenes, textos y/o fotos que suba al Sitio,
              y no se responsabiliza por el resto del material y/o contenido de
              los sitios de terceros que pudieran conectarse mediante enlaces.
              Si el Usuario decide acceder a otros sitios, lo hace a su propio
              riesgo y bajo su responsabilidad. Finca Decero adoptará las
              medidas o procedimientos oportunos para suprimir o inutilizar los
              enlaces de los que tenga conocimiento efectivo de que la actividad
              o información a la que remiten o recomiendan que sea ilícita o
              lesiona bienes o derechos de terceros susceptibles de
              indemnización. Al utilizar el Sitio, el Usuario otorga su
              consentimiento al uso que Finca Decero hace de las cookies. Si el
              Usuario no acepta que Finca Decero utilice cookies de esta manera,
              deberá configurar su explorador en consecuencia o bien no utilizar
              el Sitio. Si el Usuario deshabilita las cookies que utiliza Finca
              Decero, esto podría tener impacto en su experiencia como usuario.
              El Usuario acepta defender, indemnizar y mantener indemne a Finca
              Decero, sus directivos, empleados y representantes, de y contra
              cualquier cargo, acción o demanda, incluyendo, pero no limitándose
              a, los gastos legales razonables, que resulten del uso que el
              Usuario haga del Sitio, de sus Contenidos y de los servicios que
              se proveen por intermedio del Sitio, o bien de la infracción por
              el Usuario de los Términos y Condiciones. Finca Decero le
              notificará puntualmente cualquier demanda, acción o proceso y le
              asistirá, a su costa, en su defensa contra cualquier demanda,
              acción o proceso de esta naturaleza.
            </p>{" "}
            6.1. Los Términos y Condiciones se regirán e interpretarán de
            acuerdo con las leyes de la República Argentina. El Usuario y Finca
            Decero se someten a la jurisdicción de los Tribunales Ordinarios de
            la Ciudad de Buenos Aires, con exclusión de cualquier otro fuero o
            jurisdicción que pudiera corresponder, para el caso de cualquier
            divergencia o conflicto relacionado con el Sitio o su uso. 6.2. La
            prestación del servicio del Sitio tiene, en principio, una duración
            indefinida. No obstante ello, Finca Decero podrá dar por terminada o
            suspender la prestación del servicio del Sitio y/o de sus Contenidos
            y servicios en cualquier momento. Cuando ello sea razonablemente
            posible, Finca Decero comunicará previamente la terminación o
            suspensión de la prestación del servicio del Sitio. 6.3. El Usuario
            reconoce que el uso del Sitio y sus servicios o Contenidos está
            prohibido en todas aquellas jurisdicciones que no reconozcan la
            efectividad de los Términos y Condiciones. 6.4. Se encuentra
            totalmente prohibido copiar, pegar y/o modificar los Términos y
            Condiciones y/o los datos, imágenes y/o contenidos del Sitio. Finca
            Decero, con domicilio en San Martín 1167, piso 1, Ciudad de Mendoza,
            Provincia de Mendoza, Argentina, es el responsable del Sitio. Las
            opiniones vertidas y/o los contenidos del Sitio generados por
            terceros son de exclusiva responsabilidad de sus autores y no
            reflejan necesariamente la opinión de Finca Decero.
          </CardContent>

          <CardActions>
            <div className={classes.button}>
              <Button
                variant="contained"
                onClick={this.volver}
                color="primary"
                size="small"
              >
                Volver
              </Button>
            </div>
          </CardActions>
        </Card>
      </Fragment>
    );
  }
}

export default withStyles(styles)(TerminosYCondiciones);
