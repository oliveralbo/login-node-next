import React, { Fragment } from "react";
import Footer from "../components/Footer";
import MenuApp from "../components/MenuApp";

class Home extends React.Component {
  state = {
    usuario: ""
  };

  async componentDidMount() {
    let paquete = {};
    let conseguirUser = document.baseURI;
    let spliteado = conseguirUser.split("=");

    let userUrl = spliteado[1].split("&");
    let user = decodeURIComponent(userUrl[0]);
    let pass = decodeURIComponent(spliteado[2]);

    paquete.usuario = user;
    paquete.password = pass;

    await fetch("http://localhost:5000/login", {
      method: "post",
      body: JSON.stringify(paquete)
    })
      .then(function(respuesta) {
        // Convertir a JSON
        return respuesta.json();
      })
      .then(usuario => {
        // Ahora 'usuario' es un objeto JSON
        this.setState({ usuario });
      });
  }
  render() {
    return (
      <Fragment>
        <MenuApp usuario={this.state.usuario} />
        <Footer />
      </Fragment>
    );
  }
}
export default Home;
