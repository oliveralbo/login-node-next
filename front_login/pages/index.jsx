import React, { Fragment } from "react";
import CardLogin from "../components/CardLogin";
import Footer from "../components/Footer";

class Index extends React.Component {
  componentDidMount() {
    document.body.style.backgroundImage = 'url("./static/fondo2.jpg")';
  }
  componentWillUnmount() {
    document.body.style.backgroundImage = "initial";
  }

  render() {
    return (
      <Fragment>
        <CardLogin />

        <Footer />
      </Fragment>
    );
  }
}
export default Index;
