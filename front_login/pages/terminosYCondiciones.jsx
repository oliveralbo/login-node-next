import React, { Fragment } from "react";
import Footer from "../components/Footer";
import TerminosYCondiciones from "../components/TerminosYCondiciones";

class terminosYCondiciones extends React.Component {
  componentDidMount() {
    document.body.style.backgroundImage = 'url("./static/fondo3.jpg")';
  }
  componentWillUnmount() {
    document.body.style.backgroundImage = "initial";
  }

  render() {
    return (
      <div>
        <TerminosYCondiciones />
        <Footer />
      </div>
    );
  }
}
export default terminosYCondiciones;
