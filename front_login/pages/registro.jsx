import React, { Fragment } from "react";
import Footer from "../components/Footer";
import PanelRegistro from "../components/PanelRegistro";

class Registro extends React.Component {
  componentDidMount() {
    document.body.style.backgroundImage = 'url("./static/fondo3.jpg")';
  }
  componentWillUnmount() {
    document.body.style.backgroundImage = "initial";
  }

  render() {
    return (
      <div>
        <PanelRegistro />
        <Footer />
      </div>
    );
  }
}
export default Registro;
