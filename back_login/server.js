import express from "express";
import "babel-polyfill";
import bodyParser from "body-parser";
import cors from "cors";
import users from "./routes/users";

const app = express();
const router = new express.Router();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(users);

app.use(router);

app.listen(5000, () => console.info("Server started in port 5000!"));
