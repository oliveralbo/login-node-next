import express from "express";
import respuestaLogin from "../api/login";
import insertUser from "../api/insertUser";
import getAll from "../api/getUser";

const router = new express.Router();

router.post("/login", async (req, res) => {
  if (req.method === "POST") {
    let body = "";
    let objeto = {};
    req.on("data", chunk => {
      body += chunk.toString();
    });
    req.on("end", async () => {
      try {
        objeto = JSON.parse(body);
        let retorno = await respuestaLogin(objeto);
        res.send(retorno);
      } catch (error) {
        console.error(error);
      }
    });
  }
});

router.post("/insertarUsuario", async (req, res) => {
  if (req.method === "POST") {
    let body = "";
    let objeto = {};
    req.on("data", chunk => {
      body += chunk.toString();
    });
    req.on("end", async () => {
      try {
        objeto = JSON.parse(body);
        let retorno = await insertUser(objeto);
        res.send(JSON.stringify(retorno) + "ok");
      } catch (error) {
        console.error(error);
      }
    });
  }
});

router.get("/traerUsuarios", async (req, res) => {
  try {
    const retorno = await getAll();
    res.send(retorno);
  } catch (error) {
    res.send(console.log("fallo " + error));
  }
});

module.exports = router;
